=============
API Reference
=============

.. currentmodule:: flufl.i18n


Classes
=======

.. autoclass:: Application
   :members:

.. autoclass:: flufl.i18n._translator.Translator
   :members:

.. autoclass:: PackageStrategy
   :members:

.. autoclass:: SimpleStrategy
   :members:

.. autoclass:: flufl.i18n._registry.Registry
   :members:


Functions
=========

.. autofunction:: initialize

.. autofunction:: expand


Globals
=======

See the :class:`~_registry.Registry` class for details.

.. autodata:: registry
   :annotation:


Types
=====

.. autoclass:: RuntimeTranslator
   :members:

.. autoclass:: TranslationContextManager
   :members:

.. autoclass:: TranslationStrategy
   :members:
